import React from "react";
import "./style.css";
import image from "../logo.svg";

const Header = (props) => {
  return (
    <>
      <img className="imgStyle" src={image} />
      <h1 className="text-center text-white">Selamat datang di aplikasi {props.name}</h1>
    </>
  );
};

export default Header;
